# NOTAS #

### Para que é que isto serve? ###

* Serve para os utiliadores reportarem problemas e bugs com que se deparam para que possam ser analisados e corrigidos

### Como faço para reportar um problema? ###

* Basta clicar na opção "Issues" e depois criar um Issue reportando a situação do problema.